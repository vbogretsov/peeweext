# peeweext 0.1.0

Repository implementation on top of the [peewee](http://docs.peewee-orm.com/) and
[peewee_async](https://github.com/05bit/peewee-async) frameworks.

## Installation

~~~~{.bash}
pip install -e git+https://gitlab.com/vbogretsov/peeweext.git#egg=peeweext-0.1.0
~~~~

## Usage

It's recommended to use the [wrapt](https://pypi.python.org/pypi/wrapt) library
instead of peewee.Proxy because [peewee-async](https://github.com/05bit/peewee-async)
can't work with the last one.

~~~~{.python}
# models.py

class ProxyDb(wrapt.ObjectProxy):
    """Database proxy to allow initialize database later from config.

    We don't use peewee.Proxy because peewee_async does not support it.
    """

    def initialize(self, database):
        """Initialize database.
        """
        self.__wrapped__ = database


PROXYDB = ProxyDb(None)

class Entity(peewee.Model):
    """Base class for database entity.
    All account entities shoud be derived from this class.
    """
    id = peewee.UUIDField(primary_key=True, default=uuid.uuid4)

    class Meta:
        """Peewee Meta.
        """
        database = PROXYDB
~~~~

## Define mappings of names to repositories

~~~~{.python}
REPOSITORIES = {
    "user": lambda manager: peeweext.Repository(
        models.User,
        manager),
    # ...
}
~~~~

## Connect the database

~~~~{.python}
def connectdb(conf, loop=None):
    """Open database connection.

    Arguments:
        conf {dict} -- configuration dictionary
        loop -- ayncio event loop

    Returns:
        dabatase, storage
    """
    database = peeweext.connect(
        conf.database.driver,
        conf.database.dbname,
        **conf.database.connection)
    database.allow_sync = False
    models.PROXYDB.initialize(database)
    storage = peeweext.Storage(database, REPOSITORIES, loop)
    return database, storage
~~~~

The object storage now contains references to all the repositories listed in the
dict REPOSITORIES.

## Basic queries

~~~~{.python}

user = await storage.user.create(email="user@mail.com", pasword="123456")
user = await storage.user.get(email="user@mail.com")
await storage.user.update(user.id, email="user-2@mail.com")
await sotrage.user.delete(user.id)

~~~~

## Advanced queries

To implement additional queries in a repository just make derived class from
peeweext.Repository and implement methods for these queries.

~~~~{.python}

class ConfirmationRepository(peeweext.Repository):
    """Confirmations repository.
    """
    # pylint: disable=arguments-differ
    async def get(self, confirmation_id):
        """Get confirmation with by id.
        Arguments:
            confirmation_id {uuid.UUID} -- confirmation id
        Returns:
            {models.Confirmation} -- if confirmation with the id provided was
            found, otherwise None
        """
        try:
            return await self.manager.get((
                self.mapping
                .select(
                    self.mapping.id,
                    self.mapping.created,
                    self.mapping.expires,
                    models.User)
                .join(models.User)
                .where(self.mapping.id == confirmation_id)))
        except peewee.DoesNotExist:
            return None

~~~~

## License

BSD

# -*- coding:utf-8 -*-
"""Peewee ORM extensions.
"""

import json

import attrdict
import peewee
import peewee_async


DRIVERS = {
    "postgresql": peewee_async.PostgresqlDatabase,
    "postgresqlpool": peewee_async.PooledPostgresqlDatabase,
    "mysql": peewee_async.MySQLDatabase,
    "mysqlpool": peewee_async.PooledMySQLDatabase
}


peewee.PostgresqlDatabase


def to_dict(self, skip=()):
    """Make Peewee model serializable to {dict}.

    Arguments:
        skip {tuple} -- names to skip during serializaion to dict

    Returns:
        {peewee.Model} with the method to_dict
    """
    data = attrdict.AttrDict(self._data)
    if skip:
        for name in skip:
            del data[name]
    return data


peewee.Model.to_dict = to_dict


def connect(driver, dbname, **kwargs):
    """Connect database.

    Create database connection using pewee API.

    Arguments:
        driver {str} -- database driver, on of
            {"postgresql", "postgresqlpool"}
        dbname {str} -- database name
        **kwargs -- additioanl connection parameters
    """
    return DRIVERS[driver](dbname, **kwargs)


class JsonField(peewee.Field):
    """Represents the JSON database field.
    """

    db_field = "text"

    def __init__(self, schema=lambda x: x, **kwargs):
        super().__init__(**kwargs)
        self.schema = schema

    def db_value(self, value):
        """Convert string value from database to python dict.
        """
        return json.dumps(self.schema(value))

    def python_value(self, value):
        """Convert python dict to string.
        """
        return self.schema(json.loads(value))


class Repository:
    """Base repository implementation for single mapping.
    """

    def __init__(self, mapping, manager):
        """Initialize new Repository.

        Arguments:
            mapping {peewee.Model} -- table mapping
            manager {peewee_async.Manager} -- wrapper to execute database
                queries asynchronously
        """
        self.mapping = mapping
        self.manager = manager

    async def get(self, **criteria):
        """Get single entry that satisfies the criteria provided.

        Arguments:
            **criteria -- criteria that unique identify an entry

        Returns:
            {peewee.Model} -- entry that satisfies the criteria provided or
                None
        """
        try:
            return await self.manager.get(self.mapping, **criteria)
        except peewee.DoesNotExist:
            return None

    async def create(self, **values):
        """Create new entry.

        Arguments:
            **values -- entry fields

        Returns:
            {peewee.Model} -- new entry created
        """
        return await self.manager.create(self.mapping, **values)

    async def update(self, entry_id, **values):
        """Update entries that satisfy criteria provided.

        Arguments:
            entry_id -- id of the row to be updated
            **values == new values
        """
        await self.manager.execute(
            self.mapping.update(**values).where(self.mapping.id == entry_id))

    async def delete(self, entry_id):
        """Delete entries that satisfy criteria provided.

        Arguments:
            entry_id -- delete entry with id provided
        """
        return await self.manager.execute(
            self.mapping.delete().where(self.mapping.id == entry_id))


class Transaction:
    """Unified transaction interface.
    """

    def __init__(self, storage, native):
        self.storage = storage
        self.native = native

    def __getattr__(self, name):
        attr = getattr(self.storage, name)
        setattr(self, name, attr)
        return attr

    async def __aenter__(self):
        return await self.native.__aenter__()

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return await self.native.__aexit__(exc_type, exc_val, exc_tb)


class Storage:
    """Represents an abstract storage.
    """

    def __init__(self, database, repos, loop=None):
        """Initialize new Storage.

        Arguments:
            database {peewee.Database} -- database connection
            repos {dict} -- collection of pairs
                name: func peewee_async.Manager -> Repository
        """
        self.manager = peewee_async.Manager(database, loop=loop)
        self.repos = repos

    def __getattr__(self, name):
        repo = self.repos.get(name, None)(self.manager)
        if not repo:
            raise AttributeError(name)
        setattr(self, name, repo)
        return repo

    def atomic(self):
        """Create atomic context.

        All the databases operations inside atomic context will be executed
        in transaction.

        Returns:
            transaction context manager
        """
        return Transaction(self.manager.atomic())

# -*- coding:utf-8 -*-
"""A setuptools based setup module for authsrv.
"""

import setuptools


setuptools.setup(
    name="peeweext",
    version="0.1.0",
    description="Peewee ORM extensions.",
    url="https://gitlab.com/vbogretsov/peeweext",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    py_modules=[
        "peeweext"
    ],
    install_requires=[
        "peewee",
        "peewee_async"
    ]
)
